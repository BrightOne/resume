CC = xelatex
CV_SRCS = $(shell find cv -name '*.tex')
RESUME_SRCS = $(shell find resume -name '*.tex')

TARGETS = cv.pdf

all: $(TARGETS)

resume.pdf: resume.tex $(RESUME_SRCS)
cv.pdf: cv.tex $(CV_SRCS)
coverletter.pdf: coverletter.tex

$(TARGETS):
	$(CC) $<

.PHONY: clean
clean:
	rm -rf *.pdf
